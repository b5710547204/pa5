package clock;
/**
 * When time of alarm clock and time of current time are equal,sound of alarm will show.
 * @author Patinya Yongyai
 *
 */
public class AlarmRinging implements State{

	/**
	 * Stop sound when click plus button.
	 * @param clock is clock object
	 */
	@Override
	public void clickPlus(Clock clock) {
		clock.stopPlayingSound();
		clock.setState(clock.timeState);
	}

	/**
	 * Stop sound when click minus button.
	 * @param clock is clock object
	 */
	@Override
	public void clickMinus(Clock clock) {

		clock.stopPlayingSound();
		clock.setState(clock.timeState);
	}

	/**
	 * Stop sound when click set button.
	 */
	@Override
	public void clickSet(ClockGUI ui,Clock clock) {

		clock.stopPlayingSound();
		clock.setState(clock.timeState);
	}

	/**
	 * Do nothing.
	 */
	@Override
	public void updateTime(Clock clock) {
		/*Do nothing*/
	}
	
}
