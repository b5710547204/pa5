package clock;
/**
 * State of digital clock.
 * @author Patinya Yongyai
 *
 */
public interface State {
	/**Executed when + is pressed.*/
	public void clickPlus(Clock clock);
	/**Executed when - is pressed.*/
	public void clickMinus(Clock clock);
	/**Executed when set is pressed.*/
	public void clickSet(ClockGUI ui,Clock clock);
	/**Executed when change time between current time and alarm time*/
	public void updateTime(Clock clock);
	
}
