package clock;
import java.util.TimerTask;


/**
 * Task of clock that call updateTime() repeatedly.
 * @author Patinya Yongyai
 *
 */
public class ClockTask extends TimerTask{
	private Clock clock;
	
	/**
	 * Constructor for initial clock.
	 * @param clock is Clock.
	 */
	public ClockTask(Clock clock){
		this.clock = clock;
	}
	
	/**
	 * run() for run the code with timer (depend on interval).
	 */
	public void run(){
		clock.updateTime();
	}
	
}
