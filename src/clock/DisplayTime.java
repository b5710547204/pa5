package clock;
import java.awt.Color;

/**
 * Show time of clock.
 * @author Patinya Yongyai
 *
 */
public class DisplayTime implements State{

	/**
	 * Click plus for show time of alarm clock.
	 * @param clock to change current time to alarm time.
	 */
	@Override
	public void clickPlus(Clock clock) {
		clock.setState(clock.ringingState);
		clock.presentTime = clock.alarmTime;
	}
	

	/**
	 * Click minus for change status of alarm clock.
	 * @param clock to change status of alarm clock
	 */
	@Override
	public void clickMinus(Clock clock) {
		if(clock.alarmStatus)
			clock.alarmStatus = false;
		else
			clock.alarmStatus = true;		
	}

	/**
	 * Click set for setting time of alarm clock.
	 * @param ui to change color when setting of each state
	 * @param clock to change state to setting state
	 */
	@Override
	public void clickSet(ClockGUI ui,Clock clock) {
		clock.checkIsDisplay = false;
		clock.setState(clock.hourState);
		ui.lb_hour.setForeground(Color.red);
	}

	/**
	 * Update time to current time.
	 * @param clock to change time to current time
	 */
	@Override
	public void updateTime(Clock clock) {
		clock.presentTime = clock.date;
		
	}

}
