package clock;
/**
 * Setting Alarm State for set the alarm time.
 * @author Patinya Yongyai
 *
 */
abstract class SettingAlarm implements State{
	protected final static State SETHOUR = new SetHours();
	protected final static State SETMINUTE = new SetMinutes();
	protected final static State SETSECOND = new SetSeconds();

	/**
	 * Change present time to alarm time.
	 */
	@Override
	public void updateTime(Clock clock) {
		clock.presentTime = clock.alarmTime;
	}
}
