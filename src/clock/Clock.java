package clock;
import java.io.IOException;
import java.util.Date;
import java.util.Observable;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 * Digital clock
 * @author Patinya Yongyai
 *
 */
public class Clock extends Observable{
	protected State state;
	protected Date date;
	protected State timeState = new DisplayTime();
	protected State ringingState = new AlarmRinging();
	protected State hourState = new SetHours();
	protected State minuteState = new SetMinutes();
	protected State secondState = new SetSeconds();
	protected boolean checkIsDisplay = true;
	private Clip clip;
	protected Date presentTime;
	protected Date alarmTime;
	protected boolean alarmStatus = false;

	/**
	 * Constructor of clock.
	 */
	public Clock(){
		state = timeState;
		date = new Date();
		alarmTime = new Date();
		alarmTime.setHours(0);
		alarmTime.setMinutes(0);
		alarmTime.setSeconds(0);

	}


	/**
	 * Update time of clock and remind user from alarm clock.
	 */
	public void updateTime(){
		date.setTime(System.currentTimeMillis());
		state.updateTime(this);
		if(alarmStatus){
			if((date.getHours()==alarmTime.getHours()) && (date.getMinutes()==alarmTime.getMinutes()) && (date.getSeconds()==alarmTime.getSeconds())){
				alarmSound();
				setState(ringingState);
			}
		}
		setChanged();
		notifyObservers(presentTime);
	}

	/**
	 * Set current state to new state.
	 * @param newState is new state of clock
	 */
	public void setState( State newState ) {
		state = newState;
	}

	/**
	 * Sound of alarm clock.
	 */
	public void alarmSound(){
		AudioInputStream audioInputStream;
		try {
			audioInputStream = AudioSystem.getAudioInputStream(getClass().getResource("/sound/alarm.wav"));
			clip = AudioSystem.getClip();
			clip.open(audioInputStream);
			clip.start();
			clip.loop(Clip.LOOP_CONTINUOUSLY);
		} catch (UnsupportedAudioFileException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		} catch (LineUnavailableException e) {

			e.printStackTrace();
		}

	}

	/**
	 * Stop play sound of alarm clock.
	 */
	public void stopPlayingSound(){
		clip.stop();
	}
}
