package clock;
import java.awt.Color;

/**
 * Setting minutes of alarm clock.
 * @author Patinya Yongyai
 *
 */
public class SetMinutes extends SettingAlarm{

	/**
	 * Click plus to add minutes.
	 * @param clock to set alarm clock 
	 */
	@Override
	public void clickPlus(Clock clock) {
		clock.presentTime.setMinutes(Math.floorMod(clock.presentTime.getMinutes()+1, 60));		
	}

	/**
	 * Click minus to reduce minutes.
	 * @param clock to set alarm clock
	 */
	@Override
	public void clickMinus(Clock clock) {
		clock.presentTime.setMinutes(Math.floorMod(clock.presentTime.getMinutes()-1, 60));
	}

	/**
	 * Click set for change from minutes state to second state.
	 * @param ui to set color of label in gui
	 * @param clock to set state for clock
	 */
	@Override
	public void clickSet(ClockGUI ui,Clock clock) {
		clock.setState(clock.secondState);
		ui.lb_min.setForeground(Color.magenta);
		ui.lb_second.setForeground(Color.red);
	}


}
