package clock;
import java.awt.Color;

/**
 * Setting seconds of alarm clock.
 * @author Patinya Yongyai
 *
 */
public class SetSeconds extends SettingAlarm{

	/**
	 * Click plus to add seconds.
	 * @param clock to set alarm clock
	 */
	@Override
	public void clickPlus(Clock clock) {
		clock.presentTime.setSeconds(Math.floorMod(clock.presentTime.getSeconds()+1, 60));
	}

	/**
	 * Click minus to reduce seconds.
	 * @param clock to set alarm clock
	 */
	@Override
	public void clickMinus(Clock clock) {
		clock.presentTime.setSeconds(Math.floorMod(clock.presentTime.getSeconds()-1, 60));
		
	}

	/**
	 * Click set for change from seconds state to display time.
	 * @param ui to set color of label in gui
	 * @param clock to set state for clock
	 */
	@Override
	public void clickSet(ClockGUI ui,Clock clock) {
		clock.checkIsDisplay = true;
		clock.setState(clock.timeState);
		ui.lb_second.setForeground(Color.magenta);
	}


}
