package clock;
import java.awt.Color;

/**
 * Setting hours of alarm clock.
 * @author Patinya Yongyai
 *
 */
public class SetHours extends SettingAlarm{

	/**
	 * Click plus to add hours.
	 * @param clock to set alarm clock
	 */
	@Override
	public void clickPlus(Clock clock) {
		clock.presentTime.setHours(Math.floorMod(clock.presentTime.getHours()+1, 24));	
	}

	/**
	 * Click minus to reduce hours.
	 * @param clock to set alarm clock
	 */
	@Override
	public void clickMinus(Clock clock) {
		clock.presentTime.setHours(Math.floorMod(clock.presentTime.getHours()-1, 24));	
	}

	/**
	 * Click set for change from hours state to minutes state.
	 * @param ui to set color of label in gui
	 * @param clock to set state for clock
	 */
	@Override
	public void clickSet(ClockGUI ui,Clock clock) {
		clock.setState(clock.minuteState);

		ui.lb_hour.setForeground(Color.magenta);
		ui.lb_min.setForeground(Color.red);
	}
	
}
