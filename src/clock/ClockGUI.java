package clock;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Date;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;


/**
 * GUI of clock digital.
 * @author Patinya Yongyai
 *
 */
public class ClockGUI extends JFrame implements Observer{
	private JButton bt_set;
	private JButton bt_plus;
	private JButton bt_minus;
	protected JLabel lb_hour;
	protected JLabel lb_min;
	protected JLabel lb_second;
	private JPanel panel1;
	private JPanel panel2;
	private JPanel panel_status;
	private JPanel pn_main;
	private JLabel lb_point1;
	private JLabel lb_point2;
	private JLabel lb_alarmStatus;
	private JLabel lb_textBeforeStatus;

	private Clock clock;

	/**
	 * Constructor of ClockGUI.
	 */
	public ClockGUI(){
		super.setTitle("Cheap Digital Clock");
		initcomponent();
	}

	/**
	 * Compound of ClockGUI.
	 */
	public void initcomponent(){
		bt_set = new JButton("SET");
		bt_plus = new JButton("+");
		bt_minus = new JButton("-");
		lb_hour = new JLabel();
		lb_min = new JLabel();
		lb_second = new JLabel();
		lb_point1 = new JLabel(":");
		lb_point2 = new JLabel(":");
		lb_textBeforeStatus = new JLabel("Alarm status : ");
		lb_textBeforeStatus.setForeground(Color.white);
		lb_alarmStatus = new JLabel("OFF");
		pn_main = new JPanel();
		pn_main.setLayout(new BoxLayout(pn_main, BoxLayout.Y_AXIS));
		panel1 = new JPanel();
		panel1.setBackground(Color.black);
		lb_hour.setText("");
		lb_hour.setForeground(Color.magenta);
		lb_min.setText("");
		lb_min.setForeground(Color.magenta);
		lb_second.setText("");
		lb_second.setForeground(Color.magenta);
		lb_point1.setForeground(Color.white);
		lb_point2.setForeground(Color.white);
		panel_status = new JPanel();
		panel_status.setBackground(Color.black);
		panel_status.add(lb_textBeforeStatus);
		panel_status.add(lb_alarmStatus);

		bt_set.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				clock.state.clickSet(ClockGUI.this,clock);
			}
		});

		bt_plus.addMouseListener(new MouseAdapter() {

			public void mousePressed(MouseEvent e) {
				clock.state.clickPlus(clock);

			}

			public void mouseReleased(MouseEvent e){
				if(clock.state.getClass() == AlarmRinging.class)
					clock.setState(clock.timeState);
			}
		});

		bt_minus.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				clock.state.clickMinus(clock);
			}
		});

		panel2 = new JPanel();
		panel2.setBackground(Color.black);
		panel1.add(lb_hour);
		panel1.add(lb_point1);
		panel1.add(lb_min);
		panel1.add(lb_point2);
		panel1.add(lb_second);
		panel2.add(bt_set);
		panel2.add(bt_plus);
		panel2.add(bt_minus);
		pn_main.add(panel1);
		pn_main.add(panel_status);
		pn_main.add(panel2);
		this.add(pn_main);
		this.pack();
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

	/**
	 * 
	 * @param o is Observable (Clock)
	 * @param obj is object that send by observable.
	 */
	@Override
	public void update(Observable o, Object obj) {
		Date time = null;
		if(o.getClass() == Clock.class){
			this.clock = (Clock)o;
		}
		if(obj.getClass() == Date.class){
			time = (Date)obj;
		}
		lb_hour.setText(String.format("%02d",time.getHours()));
		lb_min.setText(String.format("%02d",time.getMinutes()));
		lb_second.setText(String.format("%02d",time.getSeconds()));
		if(clock.alarmStatus){
			lb_alarmStatus.setText("ON");
			lb_alarmStatus.setForeground(Color.green);
		}else{
			lb_alarmStatus.setText("OFF");
			lb_alarmStatus.setForeground(Color.red);
		}
	}

}
