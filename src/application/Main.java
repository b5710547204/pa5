package application;
import java.util.Timer;
import java.util.TimerTask;

import clock.Clock;
import clock.ClockGUI;
import clock.ClockTask;

/**
 * Main for run digital clock.
 * @author Patinya Yongyai
 *
 */
public class Main {
	/**
	 * 
	 * @param args not used
	 */
	public static void main(String[] args){
		Clock clock = new Clock();
		ClockGUI clockGui = new ClockGUI();
		clock.addObserver(clockGui);
		TimerTask clocktask = new ClockTask( clock );
		Timer timer = new Timer();
		long delay = 1000 - System.currentTimeMillis()%1000;
		timer.scheduleAtFixedRate(clocktask, delay,1000);
	}

}
